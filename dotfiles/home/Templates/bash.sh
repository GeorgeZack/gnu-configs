#!/bin/bash
if [ "$EUID" -ne 0 ]; then
	printf "Please run as root\n";
	exit;
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";
