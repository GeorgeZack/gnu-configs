#!/bin/bash
while getopts "ri:o:" flag; do
	case "${flag}" in
		r ) remove="true";;
		i ) input=${OPTARG};;
		o ) output=${OPTARG};;
		* ) ;;
	esac
done

printf "Input: %s\n" $input;
printf "Output: %s\n" $output;

if [[ -d $input && -d $output ]]; then
	if [[ $remove = true ]]; then
		rm -r "${output:?}"/;
	fi
	
	for img in "$input"/*; do
		pattern="^(cropped_)\w+";
		name=$(magick "$img" -ping -format "%t" info:);
		suffix=$(magick "$img" -ping -format "%e" info:);
		height=$(magick identify -ping -format '%h' "$img");
		width=$(magick identify -ping -format '%w' "$img");
		printf "img:    %s\nname:   %s\nsuffix: %s\nwidth:  %s\nheight: %s\n" $img $name $suffix $width $height;
		if ! [[ $name =~ ${pattern} ]]; then
			magick convert -verbose "$img" \
			\( -resize 1740x900\> \) \
			\( +clone \) \
			\( -clone 0 -resize 1920x1080^ -gravity center -crop 1920x1080+0+0 -blur 0x10 \) \
			-delete 0 +swap -gravity center -compose over -composite "$output/cropped_${name}.$suffix";
			printf "\n";
		fi
	done
	printf "All done.\n";
else
	tput setaf 1;
	printf "Missing argument!\n";
	tput setaf 7;
fi
