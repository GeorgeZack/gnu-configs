#!/bin/bash
if [ "$EUID" -ne 0 ]; then
	printf "Please run as root\n";
	exit;
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";
sudo bash "$SCRIPT_DIR"/rsync.sh -i /home/geo -o /mnt/backup/home;
cat "$SCRIPT_DIR"/backup.log;
