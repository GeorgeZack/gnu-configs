#!/bin/bash
if [ "$EUID" -ne 0 ]; then
	printf "Please run as root\n";
	exit;
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";

while getopts "fi:o:ev" flag; do
	case "${flag}" in
		f ) forceFlag="true";;
		i ) input=${OPTARG};;
		o ) output=${OPTARG};;
		e ) externalDriveFlag="true";;
		v ) override="true";;
		* ) ;;
	esac
done

force=false;
if [[ $forceFlag && $forceFlag == "true" ]]; then
	force=true;
fi

externalDrive=false;
if [[ $externalDriveFlag && $externalDriveFlag == "true" ]]; then
	externalDrive=true;
fi

if [[ -d $input && -d $output ]]; then
	includeFile="$SCRIPT_DIR"/include.txt;
	if [[ $externalDrive = true ]]; then
		includeFile="$SCRIPT_DIR"/include-external.txt;
	fi
	
	if [[ -f $SCRIPT_DIR/exclude.txt ]] && [[ -f "$includeFile" ]]; then
		[[ -f $SCRIPT_DIR/backup.log ]] && rm "$SCRIPT_DIR"/backup.log;
		
		if [[ ! $force = true ]]; then
			read -rp "Dry Run? (Y|n) " answer;
			case $answer in
				n|N ) dryRun="false";;
				* ) dryRun="true";;
			esac
		else
			dryRun="false";
		fi
		
		if [[ $dryRun = true ]]; then
			if [[ $override = true ]]; then
				rsync -arv -n "$input" "$output" --delete --progress >> "$SCRIPT_DIR"/backup.log;
			else
				rsync -arv -n --exclude-from="$SCRIPT_DIR"/exclude.txt --files-from="$includeFile" "$input" "$output" --delete --progress >> "$SCRIPT_DIR"/backup.log;
			fi
		else
			if [[ $override = true ]]; then
				rsync -arv "$input" "$output" --delete --progress >> "$SCRIPT_DIR"/backup.log;
			else
				rsync -arv --exclude-from="$SCRIPT_DIR"/exclude.txt --files-from="$includeFile" "$input" "$output" --delete --progress >> "$SCRIPT_DIR"/backup.log;
			fi
		fi
	else
		printf "Error:\n";
		[[ ! -f $SCRIPT_DIR/exclude.txt ]] && printf "Exclude file (%s) does not exist.\n" "exclude.txt";
		[[ ! -f "$includeFile" ]] && printf "Include file (%s) does not exist.\n" "$includeFile";
	fi
else
	printf "Bad arguments.\n"
	printf "Please add the input directory (-i DIR) and the output directory (-o DIR).\n";
fi
