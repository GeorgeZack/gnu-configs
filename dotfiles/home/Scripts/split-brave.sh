#!/bin/bash
mullvad split-tunnel pid clear;
for pid in $(pgrep brave); do
	mullvad split-tunnel pid add "$pid";
done
