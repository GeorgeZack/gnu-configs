var srcs = [];
var names = [];

var arr = document.getElementsByClassName("subpages-posts-image");
for (var i = 0; i < arr.length; i++) {
	var obj = arr[i];
	var child = obj.firstElementChild;
	var src = child.firstElementChild.src.replaceAll(/-([0-9]+x[0-9]+)/g, '');
	var split = src.split('/');
	var name = split[split.length - 1];
	srcs.push(src);
	names.push(name);
}

var interval = setInterval(download, 300, srcs);
function download(srcs) {
	var src = srcs.pop();
	var name = names.pop();
	var a = document.createElement("a");
	a.setAttribute('href', src);
	a.setAttribute('download', name);
	a.setAttribute('target', '_blank');
	a.click();

	if (srcs.length == 0) {
		clearInterval(interval);
	}
}
