#!/bin/bash
base=$(eval echo ~"$(logname)");
# for i in {0..50}; do # Test loop
pic1=$(shuf -n 1 -e "${base}"/Pictures/lotr-cropped/*);
pic2=$(shuf -n 1 -e "${base}"/Pictures/lotr-cropped/*);
cmp -s "$pic1" "$pic2" && same="true" || same="false";
while [[ $same = true ]]; do
	tput setaf 1;
	printf "DUPLICATE: %s\n" "$pic1";
	tput setaf 7;
	printf "pic1: %s\npic2: %s\n" "$pic1" "$pic2";
	pic2=$(shuf -n 1 -e ~/Pictures/lotr-cropped/*);
	cmp -s "$pic1" "$pic2" && same="true" || same="false";
	tput setaf 2;
	printf "pic1: %s\npic2: %s\n" "$pic1" "$pic2";
	tput setaf 7;
done
exec feh --no-fehbg --bg-fill "$pic1" --geometry +0+0 "$pic2" --geometry +1920+0 &
# done # End test loop
