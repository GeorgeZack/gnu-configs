#!/bin/bash
while getopts "d:s:r:f:" flag; do
	case "${flag}" in
		d ) directory=${OPTARG};;
		s ) search=${OPTARG};;
		r ) replace=${OPTARG};;
		f ) filter=${OPTARG};;
		* ) ;;
	esac
done

printf "Directory: %s\n" "$directory";
searchDir="$(realpath "$directory")";
if [[ -d $searchDir ]]; then
	files="$(grep -RiIl "$search" "$searchDir"/"$filter")";
	printf "Search: %s in %s\n" "$search" "$searchDir";
	printf "Files: %s\n" "$files";
	for f in $files; do
	    printf "File: %s\n" "$f";
	done
fi

