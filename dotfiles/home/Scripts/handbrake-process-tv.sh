#!/bin/bash
while getopts "i:o:" flag; do
	case "${flag}" in
		i ) input=${OPTARG};;
		o ) output=${OPTARG};;
		* ) ;;
	esac
done

if [[ -a "$input" && -w "$output" ]]; then
	out=$(HandBrakeCLI -t 0 -i "$input" 2>&1 >/dev/null);
	count=$(echo "$out" | grep -Eao "\\+ title [0-9]+:" | wc -l);
	for title in $(seq 1 $count); do
		HandBrakeCLI -f av_mkv -m -t "$title" -i "$input" -o "$output"/movie-"$title".mkv;
	done
fi
