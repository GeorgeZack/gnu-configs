#!/bin/bash
while getopts "i:o:" flag; do
	case "${flag}" in
		i ) input_type=${OPTARG};;
		o ) output_type=${OPTARG};;
		* ) ;;
	esac
done

pandoc_options="--columns=80"
CWD=$(pwd)

for file in "$CWD"/*; do
	if [ -f "$file" ]; then
		filename=$(basename -- "$file")
		filename_no_ext="${filename%.*}"

		if [ -f "$filename_no_ext.$input_type" ]; then
			pandoc "$filename_no_ext.$input_type" $pandoc_options -o "$CWD/$filename_no_ext.$output_type"
		fi
	fi
done
