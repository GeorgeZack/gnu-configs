#!/bin/bash
while getopts "i:o:" flag; do
	case "${flag}" in
		i ) input=${OPTARG};;
		o ) output=${OPTARG};;
		* ) ;;
	esac
done

dir=$PWD;
if [[ $output && -e $output ]]; then
	dir=$output;
fi

cd "$HOME/Downloads" || exit;

zip="$HOME/Downloads/external-library-course.zip";
if [[ $input && -e $input ]]; then
	zip=$input;
fi

files=( "$zip" );

if [[ "${#files[@]}" -eq 1 ]] && [[ -e "${files[0]}" ]]; then
	if [[ ! -d "$HOME"/Downloads/course ]]; then
		mkdir "$HOME"/Downloads/course;
		printf "Created temporary directory\n";
	fi
	if [[ ! -d "$dir"/dist ]]; then
		mkdir "$dir"/dist;
		printf "Created dist directory\n";
	else
		rm -rf "$dir"/dist/*;
		printf "Removing dist contents\n";
	fi
	printf "Unzip...\n";
	unzip "$zip" -d "$HOME"/Downloads/course;
	printf "Unzip complete\n";
	mv "$HOME"/Downloads/course/* "$dir"/dist/;
	printf "Moving to dist\n";
	rm "$zip";
	rm -r "$HOME"/Downloads/course;
	printf "Done!\n";
else
	printf "Error. There either are multiple zips or nothing matches.\n";
fi
