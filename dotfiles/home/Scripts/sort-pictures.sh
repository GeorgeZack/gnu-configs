#!/bin/bash
src="$HOME/Pictures/.TO-SORT";
dest="$HOME/Pictures";
for file in "$src"/*; do
	filename=$(basename "$file");
	year='';
	month='';
	# day='';
	if [[ "$filename" =~ ([0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{6}) ]]; then
        date=$(echo "$filename" | cut -d'-' -f2-4);
        year=$(echo "$date" | cut -c 1-4);
        month=$(echo "$date" | cut -c 6-7);
        # day=$(echo "$date" | cut -c 9-10);
	elif [[ "$filename" =~ ([0-9]{8}_[0-9]{9}) ]] || [[ "$filename" =~ ([0-9]{8}_[0-9]{6}) ]]; then
		date=$(basename "$file" | awk -F_ '{print $2}');
        year=$(echo "$date" | cut -c 1-4);
        month=$(echo "$date" | cut -c 5-6);
        # day=$(echo "$date" | cut -c 7-8);
    else
        printf "Skipping %s - Invalid filename format\n" "$file";
    fi
	dest_dir="$dest/$year/$month"
	mkdir -p "$dest_dir"
	mv "$file" "$dest_dir/"
	printf "Moved %s to %s\n" "$file" "$dest_dir"
done
