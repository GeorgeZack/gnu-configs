#!/bin/bash
if [[ -x /usr/bin/synclient && -x /usr/bin/syndaemon ]]; then
	synclient TapButton1=1;
	synclient TapButton2=3;
	synclient PalmDetect=1;
	killall syndaemon;
	exec syndaemon -K -i 0.5 -R -d;
fi
