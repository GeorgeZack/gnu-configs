#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";
while true; do
	marq_html=$(curl -s "https://forecast.weather.gov/MapClick.php?lat=35.6719&lon=-85.7921&unit=0&lg=english&FcstType=text&TextType=1");
	cur_html=$(curl -s "https://forecast.weather.gov/MapClick.php?lon=-85.79212188054422&lat=35.67187036859485");

	pos=0;
	loops=0;
	today_line=$(printf "%s" "$marq_html" | grep -E -e "Today: " -e "This Afternoon: " -m 1 | sed -n 's/.*<b>\(Today: \|This Afternoon: \)<\/b>\(.*\)<br>.*/\2/p');
	marq=$(printf " %.s" {1..30})$today_line;

	while [ "$loops" -lt 3 ]; do
		while [ "$pos" -lt "${#marq}" ]; do
			printf "%s \r" "${marq:((pos++)):30}";
			sleep 0.1;
		done
		pos=0;
		((loops++));
	done

	loops=0;
	cur_cond=$(printf "%s" "$cur_html" | grep -m 1 "myforecast-current" | sed -n 's/.*<p class="myforecast-current">\([^<]*\)<\/p>.*/\1/p');
	cur_temp=$(printf "%s" "$cur_html" | grep -m 1 "myforecast-current-lrg" | sed -n 's/.*<p class="myforecast-current-lrg">\(.*\)<\/p>.*/\1/p' | sed 's/&deg;/°/g');
	printf "%s, %s\r" "$cur_cond" "$cur_temp";
	sleep 600;
done
