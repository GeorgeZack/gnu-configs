let index = 0;
let approve = function() {
	document.querySelectorAll('.md-list [role="button"]')[index].click();
	setTimeout(() => document.querySelector('#state-toggle').click(), 500);
	setTimeout(() => document.querySelector('[role="option"][data-value="Approved"]').click(), 1000);
	setTimeout(() => document.querySelector('footer > [type="button"].md-btn--raised').click(), 1500);
	setTimeout(() => {
		index++;
		if (index < 3) {
			approve();
		}
	}, 2000);
};
approve();
