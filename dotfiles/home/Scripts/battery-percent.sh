#!/bin/bash

if [[ -x /bin/acpi ]]; then
	printf '  🔋 %4.4s' "$(acpi | grep -o '[0-9]\{1,3\}%')";
else
	printf '';
fi
