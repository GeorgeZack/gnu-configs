#!/bin/bash

volRaw=$(amixer sget Master)
volIcn=""
volStr=$(echo "$volRaw" | grep -oE '[0-9]%|[0-9][0-9]%|100%' | head -1)
volNum=$(echo "$volStr" | cut -f1 -d'%' | cut -f2 -d',' | cut -f2 -d' ')
volMute=$(echo "$volRaw" | grep -oE '\[off\]|\[on\]' | head -1)

if [[ $volMute == "[off]" ]]; then
	volIcn="🔇"
elif [[ $volNum -ge 65 ]]; then
	volIcn="🔊"
elif [[ $volNum -lt 65 && $volNum -ge 35 ]]; then
	volIcn="🔉"
else
	volIcn="🔈"
fi

printf '  %s %4.4s' "$volIcn" "$volStr";
