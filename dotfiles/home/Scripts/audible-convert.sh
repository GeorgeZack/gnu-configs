#!/bin/bash
while getopts "i:" flag; do
	case "${flag}" in
		i ) input=${OPTARG};;
		* ) ;;
	esac
done

printf "Authcode came from: https://audible-converter.ml/\n";

if [[ $input ]]; then
	AAXtoMP3 -c --target_dir ./ --authcode 1e605805 --chapter-naming-scheme 'Chapter $chapternum' $input
fi
