#!/bin/bash
for ((i=0; i<256; i++)); do
    printf "\e[48;5;${i}m%3d" "$i"
    if (( i % 16 == 15 )); then
        printf "\e[0m\n"
    fi
done
