local vim = vim
local api = vim.api
--Init plugins
vim.cmd[[ source $HOME/.config/nvim/vim-plug/plugins.vim ]]
--formatoptions must be set like this to override ftplugins
vim.cmd[[
	" filetype indent off
	filetype plugin indent off
	autocmd FileType * set formatoptions-=c
]]
vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, { pattern = '.*', command = 'setlocal fo=roqlj' })
--Rest of customizations (alphabetically sorted)
vim.cmd[[ colorscheme everforest ]]
vim.g.everforest_background = 'hard'
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
vim.opt.autoindent = true
vim.opt.background = 'dark'
vim.opt.breakindent = true
vim.opt.breakindentopt = 'sbr,list:-1'
vim.opt.colorcolumn = '+1'
vim.opt.cursorline = true
vim.opt.encoding = 'utf-8'
vim.opt.expandtab = false
vim.opt.foldenable = false
vim.opt.foldmethod = 'syntax'
vim.opt.hidden = true
vim.opt.linebreak = true
vim.opt.mouse = 'a'
-- vim.opt.wrap = false
vim.opt.wrap = true
vim.opt.number = true
vim.opt.shiftwidth = 4
vim.opt.showtabline = 0
vim.opt.sidescrolloff = 40
vim.opt.signcolumn = 'yes:2' -- With RltvNumbr
-- vim.opt.signcolumn = 'number' -- Without RltvNumbr
vim.opt.smartindent = true
vim.opt.splitright = true
vim.opt.syntax = 'enable'
vim.opt.tabstop = 4
vim.opt.termguicolors = true
vim.opt.tw = 80
vim.opt.wildignorecase = true
--Sudo write command
vim.keymap.set('', 'w!!', '<Cmd>w !sudo tee > /dev/null %', {})
--Unbind q (record)
vim.keymap.set('', 'q', '', {})
--Custom key maps
vim.keymap.set('', 'J', '10j', {})
vim.keymap.set('', 'K', '10k', {})
vim.keymap.set('', 'H', '10h', {})
vim.keymap.set('', 'L', '10l', {})
vim.keymap.set('n', '<leader>o', "o<Esc>0'_D", {})
vim.keymap.set('n', '<leader>O', "O<Esc>0'_D", {})
--Latex and Markdown spelling/linebreaking
vim.api.nvim_create_autocmd({ 'BufRead', 'BufNewFile' }, { pattern = { '*.tex', '*.md', '*.txt' }, command = 'setlocal wrap linebreak spell spelllang=en_us noexpandtab formatlistpat=^\\s*\\d\\+[.)]\\s\\+\\|^\\s*[*+~-]\\s\\+\\|^\\(\\|[*#]\\)\\[^[^\\]]\\+\\]:\\s comments=n:>' })

--CtrlSpace setup
vim.g.CtrlSpaceDefaultMappingKey = '<C-space> '

--TreeSitter setup
require('nvim-treesitter.configs').setup({
	ensure_installed = {
		'bash',
		'c',
		'cpp',
		'css',
		'go',
		-- 'help',
		'html',
		'javascript',
		'json',
		'latex',
		'lua',
		'markdown',
		'python',
		'toml',
		'vim',
		'yaml',
	},
	auto_install = true,
	indent = {
		-- enable = true
		disable = true
	}
})

--Blankline setup
--Adds indentation guides to all lines + attempts to use treesitter :/
vim.opt.list = true
require('ibl').setup({
	indent = {
		char = '|',
		tab_char = '|'
	},
	scope = {
		show_start = false,
		show_end = false,
		highlight = { 'IndentBlanklineContextChar' }
	}
})
local hooks = require('ibl.hooks')
hooks.register(hooks.type.WHITESPACE, hooks.builtin.hide_first_space_indent_level)
hooks.register(hooks.type.WHITESPACE, hooks.builtin.hide_first_tab_indent_level)

--Airline setup
--Show buffers on top of window + theme
vim.cmd[[
	let g:airline_powerline_fonts = 1
	let g:airline#extensions#tabline#enabled = 1
	let g:airline#extensions#tabline#buffer_nr_show = 0
	let g:airline#extensions#tabline#buffer_idx_mode = 1
	let g:airline#extensions#tabline#show_tabs = 0
	let g:airline_theme='everforest'
	let g:airline#extensions#ctrlspace#enabled = 1
	let g:airline#extensions#tabline#ctrlspace_show_tab_nr = 0
]]

--Airline + CtrlSpace setup
vim.cmd[[ let g:CtrlSpaceStatuslineFunction = 'airline#extensions#ctrlspace#statusline()' ]]

--Vimtex setup
vim.cmd[[
	let g:vimtex_view_general_viewer = 'qpdfview'
	let g:vimtex_view_general_options = '--unique @pdf\#src:@tex:@line:@col'
]]

--Setup LSP
require('lspconfig').util.default_config = vim.tbl_extend(
	'force',
	require('lspconfig').util.default_config, {
		on_attach = on_attach
	}
)
require('mason').setup({
	log_level = vim.log.levels.DEBUG
})

--Suppress LSP hints
vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
	vim.lsp.diagnostic.on_publish_diagnostics, {
		underline = {
			severity_limit = 'Warning',
		},
		update_in_insert = false,
		virtual_text = false,
	}
)
vim.lsp.set_log_level('off')

--Modifying gutter for use with LSP
vim.diagnostic.config({
	signs = true,
	severity_sort = false,
})

--Setup each LSP server
local lsp_capabilities = vim.tbl_deep_extend('force',
	vim.lsp.protocol.make_client_capabilities(),
	require('cmp_nvim_lsp').default_capabilities()
)
lsp_capabilities.workspace.didChangeWatchedFiles.dynamicRegistration = false

-- local lfs = require('lfs')
-- function getLatestVersion(path)
-- 	local latestVersion
-- 	local latestTimestamp = 0
-- 
-- 	for entry in lfs.dir(path) do
-- 		if entry ~= '.' and entry ~= '..' then
-- 			local fullPath = path .. '/' .. entry
-- 			local attributes = lfs.attributes(fullPath)
-- 
-- 			if attributes and attributes.mode == 'directory' and attributes.change > latestTimestamp then
-- 				latestTimestamp = attributes.change
-- 				latestVersion = entry
-- 			end
-- 		end
-- 	end
-- 
-- 	return latestVersion
-- end
-- 
-- local homeDir = os.getenv('HOME')
-- local nodeVersionPath = homeDir .. '/.nvm/versions/node'
-- local latestNodeVersion = getLatestVersion(nodeVersionPath)

require('mason-lspconfig').setup_handlers({
	-- The first entry (without a key) will be the default handler
	-- and will be called for each installed server that doesn't have
	-- a dedicated handler.
	function (server_name) -- default handler (optional)
		-- ESLint configuration
		if server_name == 'eslint' then
			-- local eslint_config = homeDir .. '/.eslintrc.js'
			--
			-- require('lspconfig').eslint.setup({
			-- 	capabilities = lsp_capabilities,
			-- 	root_dir = function(fname)
			-- 		return require('lspconfig').util.find_git_ancestor(fname)
			-- 	end,
			-- 	-- cmd = { 'vscode-eslint-language-server', '--stdio' --[[ '--stdin', '--stdin-filename', '%filepath' ]] },
			-- 	-- cmd = { 'eslint', '--stdin', '--stdin-filename', '%filepath' },
			-- 	filetypes = { 'javascript', 'javascriptreact' },
			-- 	settings = {
			-- 		debug = true,
			-- 		rootMarkers = { '.git/' },
			-- 		languages = {
			-- 			javascript = { eslint_config },
			-- 			javascriptreact = { eslint_config },
			-- 		},
			-- 		workingDirectory = { mode = 'auto' },
			-- 		options = {
			-- 			overrideConfigFile = eslint_config,
			-- 		},
			-- 		-- nodePath = homeDir .. '/.nvm/versions/node/' .. latestNodeVersion .. '/lib/node_modules/eslint/lib/api.js',
			-- 		nodePath = homeDir .. '/.nvm/versions/node/' .. latestNodeVersion .. '/lib/node_modules/'
			-- 	},
			-- 	libs = { homeDir .. '/.nvm/versions/node/' .. latestNodeVersion .. '/lib/node_modules/' },
			-- })
			return
		else
			require('lspconfig')[server_name].setup({
				capabilities = lsp_capabilities,
				root_dir = function(fname)
					return require('lspconfig').util.find_git_ancestor(fname)
				end,
			})
		end
	end,
})

--Setup DAP
-- require('mason-nvim-dap').setup()

--No-config GO/Delve DAP setup
-- require('dap-go').setup()

--Setup Completion
--See https://github.com/hrsh7th/nvim-cmp#basic-configuration
local cmp = require'cmp'
cmp.setup({
	-- Enable LSP snippets
	snippet = {
		expand = function(args)
			vim.fn['vsnip#anonymous'](args.body)
		end,
	},
	mapping = {
		-- Add tab support
		['<S-Tab>'] = cmp.mapping.select_prev_item(),
		['<C-Tab>'] = cmp.mapping.select_next_item(),
		['<C-d>'] = cmp.mapping.scroll_docs(-4),
		['<C-f>'] = cmp.mapping.scroll_docs(4),
		['<C-Space>'] = cmp.mapping.complete(),
		['<C-e>'] = cmp.mapping.close(),
		['<C-l>'] = cmp.mapping.confirm({
			behavior = cmp.ConfirmBehavior.Insert,
			select = true,
		})
	},
	-- Installed sources
	sources = {
		{ name = 'nvim_lsp' },
		{ name = 'vsnip' },
		{ name = 'path' },
		{ name = 'buffer' },
	},
	view = {
		entries = 'custom'
	},
})

--Trouble setup
require('trouble').setup {
	height = 8,
	padding = false,
}
vim.keymap.set('n', '<leader>r', '<Cmd>TroubleToggle<CR>', { silent = true, noremap = true })

--Comment setup
require('Comment').setup()

--Telescope setup
require('telescope').setup({
	defaults = {
		file_ignore_patterns = {
			'node_modules/.',
			'.git/.'
		}
	},
	extensions = {
		file_browser = {
			follow_symlinks = true,
			grouped = true,
			hidden = { file_browser = true, folder_browser = true },
			hijack_netrw = true,
			initial_mode = 'normal',
			theme = 'ivy',
			mappings = {
				['n'] = {
					['J'] = function(prompt_bufnr)
						require('telescope.actions.set').shift_selection(prompt_bufnr, 10)
					end,
					['K'] = function(prompt_bufnr)
						require('telescope.actions.set').shift_selection(prompt_bufnr, -10)
					end,
				}
			}
		}
	}
})
require('telescope').load_extension 'file_browser'
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', '<Cmd>Telescope find_files hidden=true<CR>', {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<space>fb', '<Cmd>Telescope file_browser<CR>', {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})
-- vim.keymap.set('n', '<leader>t', '<Cmd>Telescope diagnostics<CR>', {})

--RltvNumbr config (not to be confused with Hybrid or Relative line numbers)
--POOR PERFORMANCE!
vim.cmd[[
	hi default HL_RltvNmbr_Minus	gui=none	guifg=gray29	guibg=none
	hi default HL_RltvNmbr_Positive	gui=none	guifg=gray29	guibg=none
	autocmd BufRead,BufNewFile * RN
]]

--Markdown Preivew
vim.cmd[[ let g:mkdp_auto_close = 0 ]]
vim.keymap.set('n', '<leader>mp', '<Cmd>MarkdownPreview<CR>', {})

--Vim Markdown??
-- vim.cmd[[
-- 	let g:vim_markdown_auto_insert_bullets=0
-- 	let g:vim_markdown_new_list_item_indent=0
-- ]]

--Arial setup (for code outlining/navigation)
require('aerial').setup({
	layout = {
		default_direction = "prefer_left",
		width = 30
	},
	on_attach = function(bufnr)
		vim.keymap.set('n', '{', '<Cmd>AerialPrev<CR>', {buffer = bufnr})
		vim.keymap.set('n', '}', '<Cmd>AerialNext<CR>', {buffer = bufnr})
	end
})
vim.keymap.set('n', '<leader>a', '<Cmd>AerialToggle!<CR>')

--Telescope fix - leaving telescope window would change to insert mode rather than stay in normal mode
vim.api.nvim_create_autocmd('WinLeave', {
	callback = function()
		if vim.bo.ft == 'TelescopePrompt' then
			if vim.fn.mode() == 'i' then
				vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Esc>', true, false, true), 'i', false)
			end
		end
	end,
})

--Bullets (list support)
vim.cmd[[ let g:bullets_outline_levels = [] ]]
