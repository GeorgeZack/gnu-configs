" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
	\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	"autocmd VimEnter * PlugInstall
	"autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')
	Plug 'vim-scripts/RltvNmbr.vim' "Hybrid relative numbers
	Plug 'sheerun/vim-polyglot' "Language support
	" Plug 'kyazdani42/nvim-tree.lua' "File viewer
	Plug 'kyazdani42/nvim-web-devicons' "Extra icons for NvimTree
	Plug 'williamboman/mason.nvim', { 'do': ':MasonUpdate' } "LSP language server(s) installer via Mason
	Plug 'williamboman/mason-lspconfig.nvim' "LSP config + Mason bridge
	Plug 'neovim/nvim-lspconfig' "LSP configurations
	Plug 'hrsh7th/nvim-cmp' "Completion framework
	Plug 'hrsh7th/cmp-nvim-lsp' "LSP completion for nvim-cmp
	Plug 'hrsh7th/cmp-path' "System paths for nvim-cmp
	Plug 'hrsh7th/cmp-buffer' "Buffer words for nvim-cmp
	Plug 'hrsh7th/cmp-vsnip' "Snippet completion for nvim-cmp
	Plug 'hrsh7th/vim-vsnip' "Snippet engine
	Plug 'folke/trouble.nvim' "Trouble (diagnostic display for LSP etc.)
	Plug 'sainnhe/everforest' "Theme
	Plug 'vim-ctrlspace/vim-ctrlspace' "Extended buffer/workspace navigation
	Plug 'vim-airline/vim-airline' "Cool info bar
	Plug 'vim-airline/vim-airline-themes' "Cooler themes for bar
	Plug 'nvim-treesitter/nvim-treesitter', { 'do': ':TSUpdate' } "Tree folding/highlighting
	Plug 'lukas-reineke/indent-blankline.nvim' "Indentation guides
	Plug 'Pocco81/true-zen.nvim' "Zen mode for writing
	Plug 'lervag/vimtex' "LaTeX functionality/helpers
	Plug 'nvim-lua/plenary.nvim' "Plenary required for vgit + telescope
	Plug 'tanvirtin/vgit.nvim' "VGit
	Plug 'nvim-telescope/telescope.nvim' "Teslecope fuzzy finder
	Plug 'nvim-telescope/telescope-file-browser.nvim' "Teslecope fuzzy finder
	Plug 'numToStr/Comment.nvim' "Commenting support
	" Plug 'mfussenegger/nvim-dap' "Debug Adapter Protocol
	" Plug 'jay-babu/mason-nvim-dap.nvim' "Mason/DAP bridge
	" Plug 'leoluz/nvim-dap-go' "No config Go DAP setup
	Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && nvm use --lts && npm install' } "Markdown preivew
	Plug 'stevearc/aerial.nvim' "Markdown/Treesitter/symbols management/navigation
	Plug 'dhruvasagar/vim-table-mode' "Markdown tables
	Plug 'dkarter/bullets.vim' "Markdown lists
call plug#end()
