#!/bin/bash
# ~/.bashrc
[[ $- != *i* ]] && return

# shellcheck disable=SC1091
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

use_color=true
# Set colorful PS1 only on colorful terminals. dircolors --print-database uses
# its own built-in database instead of using /etc/DIR_COLORS. Try to use the
# external file first to take advantage of user additions. Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if [[ "${use_color}" ]]; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval "$(dircolors -b ~/.dir_colors)"
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval "$(dircolors -b /etc/DIR_COLORS)"
		fi
	fi

	if [[ "${EUID}" == 0 ]]; then
		# EMBEDDED_PS1='\[\033[38;5;196;1m\]root\[\033[38;5;15;1m\]@\[\033[01;36m\]\h\[\033[01;37m\] \W\[\033[00m\]'
		EMBEDDED_PS1='\[\033[38;5;196;1m\]root\[\033[0;37m\]@\[\033[38;5;105;1m\]\W\[\033[00m\]'
	else
		# EMBEDDED_PS1='\[\033[38;5;76;1m\]\u\[\033[38;5;15;1m\]@\[\033[01;36m\]\h\[\033[01;37m\] \W\[\033[00m\]'
		EMBEDDED_PS1='\[\033[38;5;76;1m\]\u\[\033[0;37m\]@\[\033[38;5;105;1m\]\W\[\033[00m\]'
	fi

	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
else
	if [[ "${EUID}" == 0 ]]; then
		# EMBEDDED_PS1='\u@\h \W'
		EMBEDDED_PS1='\u@\W'
	else
		# EMBEDDED_PS1='\u@\h \w'
		EMBEDDED_PS1='\u@\w'
	fi
fi

# vi mode
# set -o vi
# bind "set show-mode-in-prompt on"

reset_readline_prompt_mode_strings() {
	# local show_mode
	# bind -v | grep -q 'set show-mode-in-prompt on' && show_mode=1 || show_mode=0
	# if [[ "$show_mode" == 1 ]]; then
	# 	bind "set vi-cmd-mode-string "
	# 	bind "set vi-ins-mode-string "
	# 	local vi_cmd vi_ins
	# 	if [[ "${use_color}" ]]; then
	# 		vi_cmd='\[\033[38;5;214m\]:\[\033[00m\]'
	# 		vi_ins='\[\033[38;5;64m\]$\[\033[00m\]'
	# 	else
	# 		vi_cmd=':'
	# 		vi_ins='$'
	# 	fi
	# 	local vi_mode vi_str
	# 	bind -v | grep -q 'set keymap vi-insert' && vi_mode=1 || vi_mode=0
	# 	if [[ "$vi_mode" == 1 ]]; then
	# 		vi_str=$vi_ins
	# 	else
	# 		vi_str=$vi_cmd
	# 	fi
	# 	PS1="${EMBEDDED_PS1}${vi_str} "
	# else
	# fi
	local user_name
	if [[ "${EUID}" == 0 ]]; then
		user_name='root'
	else
		user_name="${USER}"
	fi
	local window_title
	case ${TERM} in
		st*)		window_title="\033]0;${user_name}:${PWD/#$HOME/\~}\007";;
		screen*)	window_title="\033_${user_name}:${PWD/#$HOME/\~}\033\\";;
	esac
	echo -ne "$window_title"
}

PS1="${EMBEDDED_PS1} "
unset use_color safe_term match_lhs sh

xhost +local:root > /dev/null 2>&1

complete -cf sudo

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

# Custom Bindings:
bind 'set completion-ignore-case on'
bind '"\e[1;5D" backward-word'
bind '"\e[1;5C" forward-word'
bind '"\e[1;5C" forward-word'
bind 'set mark-symlinked-directories on'

# Custom Functions:
targz() {
	# usage: targz <file/directory>
	tar -zcvf "$1.tar.gz" "$1"; rm -r "$1";
}
untargz() {
	# usage: untargz <file.tar.gz>
	tar -zxvf "$1"; rm -r "$1";
}
ex() {
	# usage: ex <file>
	if [ -f "$1" ] ; then
		case $1 in
			*.tar.bz2)	tar xjf "$1";;
			*.tar.gz)	tar xzf "$1";;
			*.bz2)		bunzip2 "$1";;
			*.rar)		unrar x "$1";;
			*.gz)		gunzip "$1";;
			*.tar)		tar xf "$1";;
			*.tbz2)		tar xjf "$1";;
			*.tgz)		tar xzf "$1";;
			*.zip)
				if [ -f /bin/unar ]; then
					echo "unar";
					unar "$1";
				else
					echo "unzip";
					unzip "$1";
				fi
				;;
			*.Z)		uncompress "$1";;
			*.7z)		7z x "$1";;
			*)			echo "'$1' cannot be extracted via ex()";;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}

# Custom Aliases:
alias air='$HOME/go/bin/air'
alias bc="bc -l"
alias cal3="cal -n 3"
alias cozy="com.github.geigi.cozy"
alias cp='cp -i'
alias df='df -h'
alias du="du -sh"
alias exa="eza -lha --group-directories-first"
alias eza="eza -lha --group-directories-first"
alias feh="nsxiv -a"
alias listzip="unzip -l"
alias more=less
alias mpg123="mpg123 -v"
alias mpv="mpv --term-osd-bar"
alias nsxiv="nsxiv -a"

# Beyond All Reason:
if [[ -d "$HOME/Games/bar" ]]; then
	alias bar='exec "$HOME/Games/bar/run.AppImage"'
fi

# Brutal Doom
if [[ -d "$HOME/Games/brutal-doom" && -x "/bin/gzdoom" && -d "$HOME/Games/doom-wads" ]]; then
	alias brutal1='gzdoom -iwad "$HOME/Games/doom-wads/DOOM.WAD" -file "$HOME/Games/brutal-doom/brutalv21.pk3"';
	alias brutal2='gzdoom -iwad "$HOME/Games/doom-wads/DOOM2.WAD" -file "$HOME/Games/brutal-doom/brutalv21.pk3"';
fi

# Xonotic:
if [[ -d "$HOME/Games/Xonotic" ]]; then
	alias xonotic="bash -c '\$HOME/Games/Xonotic/xonotic-linux-glx.sh -sessionid xonoticgame'"
	if [[ -f "$HOME/.xonotic/data/server.cfg" ]]; then
		alias xonotic-server="bash -c '\$HOME/Games/Xonotic/server_linux.sh -sessionid xonoticserver'"
	fi
fi

# Custom Exports:
export EDITOR=nvim
export BROWSER=/usr/bin/brave

# Define the base PATH
BASE_PATH="$HOME/.local/bin:$HOME/Scripts/:"

# Initialize an empty PATH variable
NEW_PATH=""

# Conditionally add each directory to NEW_PATH
add_to_path() {
    if [ -d "$1" ]; then
        NEW_PATH="$NEW_PATH:$1"
    fi
}

add_to_path "$BASE_PATH"
add_to_path "$HOME/.config/nvim/autoload/plugged/nvim-treesitter/"
add_to_path "$HOME/.local/bin"
add_to_path "$HOME/.local/share/nvim/lsp_servers/"
add_to_path "$HOME/.local/share/nvim/mason/bin"
add_to_path "$HOME/.local/share/gem/ruby/3.0.0/bin"
add_to_path "/usr/lib/jvm/java-17-jdk/bin"

# Find the latest version of Node.js installed via NVM
if [[ -e "$HOME/.nvm/versions/node/" ]]; then
	latest_node_version=$(ls -t "$HOME/.nvm/versions/node/" | head -n 1)
	add_to_path "$HOME/.nvm/versions/node/$latest_node_version/bin"
	add_to_path "$HOME/.nvm/versions/node/$latest_node_version/lib/node_modules"
fi

# Remove any leading ":" and set the final PATH
export PATH="${NEW_PATH#:}:$PATH"
export NODE_PATH="$HOME/nvm/versions/node/$latest_node_version/lib/node_modules"
export PROMPT_COMMAND=reset_readline_prompt_mode_strings

if [[ -e "/usr/share/nvm/init-nvm.sh" ]]; then
	# shellcheck source=/dev/null
	source /usr/share/nvm/init-nvm.sh
fi

set | grep -q allexport && printf "There is a set -a in a script somewhere.\n"
