if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
	exec startx
fi

# Created by `pipx` on 2023-01-25 23:53:18
export PATH="$PATH:/home/geo/.local/bin"
