# Welcome to Artix
## A guide
George Zackrison
Originally written: 02/01/2021; Last revision: 01/09/2024

A guide to walk you through installing and successfully running Artix (the based
version of Arch).

## Notes

<https://bugs.archlinux.org/task/44276> (See comment \#3, by Lex Onderwater
(ReLaxLex) on Monday, 23 March 2015)

### Artix installation process:

<https://wiki.artixlinux.org/Main/Installation> (Important: visit Network
configuration before turning off the machine first!)

<https://davidtsadler.com/posts/arch/>

### Artix connmanctl:

	$ rfkill unblock wifi; connmanctl enable wifi; connmanctl scan wifi; connmanctl
	> agent on
	> connect [wifi ssid]

### Installing on a Dell XPS

Current setup that works is as follows:
- Partition 1
	- EFI System
	- 315MB
	- FAT32
	- flags include `boot` `esp`
	- no label
- Partition 2
	- Linux Filesystem
	- Rest of drive
	- EXT4
	- no flags
	- label `ROOT`

The drive is GPT, with the boot mounted at `/boot/efi`. The Grub configuration
file is located at `/etc/default/grub`. Use `$ update-grub` to update the Grub
configuration.

### Runit service:

<https://medium.com/100-days-of-linux/setting-up-a-functional-runit-service-be75b5bc692c>

### Runit Service Manager:

<https://gitea.artixlinux.org/linuxer/Runit-Service-Manager>

### Network configuration:

<https://www.cyberciti.biz/faq/linux-list-network-interfaces-names-command/>

<https://wiki.artixlinux.org/runit/NetworkConfiguration>

<https://linuxhint.com/arch_linux_network_manager/>

#### NetworkManager

List nearby wifi

	$ nmcli device wifi list

Connecting to wifi

	$ nmcli device wifi connect <SSID> password <SSID_password>

List all connections

	$ nmcli connection show

Device status

	$ nmcli device

Turn networking on or off
	
	$ nmcli networking [on/off]

### Adding users:

<https://davidtsadler.com/posts/arch/2020-06-15/adding-a-user-in-arch-linux/>

### Making users sudo:

<https://davidtsadler.com/posts/arch/2020-06-22/granting-sudo-access-to-a-user-in-arch-linux/>

### How to work dwm:

<https://ratfactor.com/dwm>

or

<https://dwm.suckless.org/tutorial/>

### Repo home:

<https://codeberg.org/GeorgeZack/gnu-configs>

## Things that are good to know

### ST Keyboard Shortcuts

Increase font size:

	Ctrl+Shift+PageUp

### DWM Keyboard Shortcuts

Disable/enable meta key:

	fn+meta

### Using .AppImage files

Simple! Use:

	$ chmod a+x <AppImage file> +
	$ ./<AppImage file>

### Applying a DIFF File

	$ patch -p1 < <diff file>

### CHMOD Chart

![chmod](chmod.png)

### Applying a patch to an already existing WINE application

	$ WINEPREFIX="path/to/wine/directory" wine64 start /unix
	"path/to/patch.exe"

For Steam games, it's best to move the `.exe` file into the Wine directory of
the game, this is located in the
`.steam/steam/steamapps/compatdata/[GAMEID]/pfx` directory (usually in
`drive_c`. It's also best to point to the `pfx` directory when setting the
`WINEPREFIX`.

### Install LaTeX CTAN Package via tllocalmgr

To check where your $TEXMFLOCAL directory is:

	$ kpsewhich --var-value=TEXMFLOCAL

#### START OUTDATED INFO
To install a CTAN package:

	$ tllocalmgr install <package-name>
#### END OUTDATED INFO

### Start process in background and disown it

	$ <program> &

Resulting execution will yield a job id...

	$ disown <job id>

### Restarting DWM after updating the config

	`<meta>+<ctrl>+q`

### Printing

Setup:

	$ system-config-printer

List available printers:

	$ lpstat -p -d

Print from command line:

	$ lp <filename>

Prints 5 copies of a file that is set to "fit-to-page" from printer "LaserJet":

	$ lp -n 5 -o fit-to-page -d LaserJet <filename>

Prints pages 1 and 2 of a file that is set to "fit-to-page", in draft quality,
and double-sided from printer "LaserJet":

	$ lp -o fit-to-page -o sides=two-sided-long-edge -o print-quality=3 -P 1,2
	-d LaserJet <filename>

### NSXIV Commands

`n` to go to **next** image and `p` to go to the **previous** image. Adding `-t`
to the command will open in thumbnail mode.

`<enter>`/`<return>` key to enter thumbnail mode.

### Insert special characters / Compose key

`setxkbmap -option compose:rctrl` to set the `Compose` key to `Right Ctrl`, from
there you can use Compose with other keys to produce a new character.

## Programs + Clients

### Office

`libreoffice-still` though `-fresh` sometimes works better... idk

### Music

`clementine`

### Audiobooks

`cozy` - `.bashrc` aliases `cozy` from `com.github.geigi.cozy`

### Torrent Files

`transmission`/`transmission-gtk`

### Audio CD

`abcde` to load and rip Audio CD's.

`mp3info` for mp3 tag management.

### Movie DVDs/Blue-Rays

`HandBrakeCLI` to turn DVDs into video files. The example below will copy the
main feature film (`--main-feature`), add chapter markers (`-m`), copy the audio
encoder (`-E copy`) use the audio language on track three (`-a 3`) and subtitle
track one (`-s 1`) to produce an `mkv` file (`-f av_mkv`)

	$ HandBrakeCLI -i /movie/VIDEO_TS -o /movie/output.mkv -f av_mkv -m -E copy
	--main-feature -a 3 -s 1

### Download Images from Camera

`gphoto2`

### Scan/Use the Scanner

`sudo scanimage -d pixma:04A91912_4971A4 -v > [file].tiff`

`sudo scanimage --list-devices` to find device
