#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";
source "$SCRIPT_DIR/conf"

while getopts "cehr" flag; do
	case "${flag}" in
		c )	flagClean="true";;
		e )	flagEtc="true";;
		h )	flagHome="true";;
		r )	flagRunit="true";;
		* )	;;
	esac
done

if [[ $flagClean == "true" ]]; then clean=true; fi
if [[ $flagEtc == "true" ]]; then copyEtc=true; fi
if [[ $flagHome == "true" ]]; then copyHome=true; fi
if [[ $flagRunit == "true" ]]; then copyRunit=true; fi

function link {
	file=$(basename "$1");
	path="$dirHome";
	basePath="$base";
	doNotLink=false;

	if [[ $2 = true ]]; then
		path="$dirHome"/.config;
		basePath="$base"/.config;
	fi

	for check in $excluded; do
		if [[ $file == "$check" ]]; then
			doNotLink=true;
		fi
	done

	if [[ ! $file = "." && ! $file = ".." && $doNotLink = false ]]; then
		if [[ ( -f $path/$file || -d $path/$file ) && ( ! -f $basePath/$file && ! -d $basePath/$file ) && ! $file = ".config" ]]; then
			$textG;
			printf "Linking %s...\n" "$file";
			$textR;
			ln -s "$path"/"$file" "$basePath"/;
			$textW;
			chown -h "$user":"$user" "$basePath"/"$file";
		elif [[ ( -f $path/$file || -d $path/$file ) && $file = ".config" ]]; then
			for a in "$path"/.config/*; do
				link "$a" true;
			done
			for a in "$path"/.config/.*; do
				link "$a" true;
			done
		fi
	fi
	$textW;
}

if [[ $copyEtc ]]; then
	printf "Replacing /etc/environment...\n";
	$textR;
	if [[ -f /etc/environment && ! -f /etc/environment.old ]]; then
		mv /etc/environment /etc/environment.old;
	fi
	if [[ -f /etc/environment && -f /etc/environment.old ]]; then
		mv /etc/environment /etc/environment.old;
	fi
	ln -s "$dirEtc"/environment /etc/;
	chown -R root:root /etc/environment;
	$textW;
fi

if [[ $clean ]]; then
	$textC;
	printf "Running cleanup script...\n";
	$textW;
	bash "$SCRIPT_DIR"/cleanup.sh "$base";
fi

shopt -s dotglob;
if [[ $copyHome && -d "$dirHome"/ ]]; then
	if [[ ! -d $base/.config ]]; then
		mkdir "$base"/.config;
		chown -R "$user":"$user" "$base"/.config;
		$textY;
		printf ".config does not exist, creating...\n";
		$textW;
	fi
	for a in "$dirHome"/*; do
		link "$a" false;
	done
	for a in "$dirHome"/.*; do
		link "$a" false;
	done
	$textC;
	printf "Files linked!\n\n";
	$textW;
fi
shopt -u dotglob;

if [[ $copyRunit && -d "$dirRunit"/ ]]; then
	$textR;
	if [[ ! -d "/etc/runit" ]]; then
		mkdir /etc/runit;
	fi
	if [[ ! -d "/run/runit" ]]; then
		mkdir /run/runit;
	fi
	if [[ ! -d "/run/runit/serivce" ]]; then
		mkdir /run/runit/service;
		ln -sf /etc/runit/runsvdir/current /run/runit/service;
	fi
	for file in "$dirRunit"/*; do
		fileName=$(basename "$file");
		$textG;
		printf "Linking %s to /usr/local/bin/...\n" "$fileName";
		$textR;
		chown -h "${user:=$(/usr/bin/id -run)}":"$user" "$file";
		chmod +x "$file";
		ln -sf "$file" /etc/runit/sv/;
		$textW;
		rsm enable "$fileName";

		if [[ $force = false ]]; then
			read -rp "Start service $fileName? (Y|n) " answer;
			case $answer in
				n|N )	;;
				* )		rsm start "$fileName";;
			esac
		else
			rsm start "$fileName";
		fi
	done
	$textC;
	printf "Custom runit services linked!\n\n";
	$textW;
fi
