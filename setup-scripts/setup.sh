#!/bin/bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";
source "$SCRIPT_DIR/conf"

while getopts "o:fsbrh" flag; do
	case "${flag}" in
		o )	overrideFlag=${OPTARG};;
		f )	forceFlag="true";;
		s ) skipFlag="true";;
		b ) barebonesFlag="true";;
		r ) readingMachineFlag="true";;
		# v ) verboseFlag="true";; # TODO: Implement option
		h ) helpFlag="true";;
		* )	;;
	esac
done

if [[ $helpFlag = true ]]; then
	bold=$(tput bold);
	norm=$(tput sgr0);
	printf "Setup script help\n";
	printf "\n";
	printf "Options:\n";
	printf "	%s-o [user]	O%sverride flag. If the %s-o%s option, and followed by a
			name, the setup will override the current user with supplied
			value. Assumes that the supplied value/user is present on the
			system.\n" "$bold" "$norm" "$bold" "$norm";
	printf "	%s-f		F%sorce flag. If the %s-f%s option is present, the setup will
			not ask for confirmation during setup.\n" "$bold" "$norm" "$bold" "$norm";
	printf "	%s-s		S%skip updates flag. If the %s-s%s option is present, the
			setup will skip updates.\n" "$bold" "$norm" "$bold" "$norm";
	printf "	%s-b		B%sarebones flag. If the %s-b%s option is present, the setup
			only setup barebones configurations and packages. Used on
			servers or systems that only need some customizations.\n" "$bold" "$norm" "$bold" "$norm";
	printf "	%s-r		R%seading machine flag. If the %s-r%s option is present, the
			setup only setup configurations and packages for a device
			aimed at reading and editing text/asciidoc files. Used on
			laptops or systems that only need some customizations.\n" "$bold" "$norm" "$bold" "$norm";
	#printf "	%s-v		V%serbose flag. If the %s-v%s option is present, the setup
	#		will print more information throughout the setup.\n" "$bold" "$norm" "$bold" "$norm";
	printf "	%s-h		H%selp flag. Displays this dialog.\n" "$bold" "$norm";
	printf "\n";
	exit;
fi

source "$SCRIPT_DIR/conf"

function link {
	file=$(basename "$1");
	path="$dirLink";
	basePath="$base";
	excluded="";
	doNotLink=false;
	
	if [[ $2 = true ]]; then
		path="$dirLink"/.config;
		basePath="$base"/.config;
	fi
	
	if [[ $barebones = true ]]; then
		excluded=".icons .newsboat autostart gtk-2.0 gtk-3.0 mpd mpv ncmpcpp Templates wise-gnu.cow";
		for check in $excluded; do
			if [[ $file == "$check" ]]; then
				doNotLink=true;
			fi
		done
	fi
	
	if [[ $readingMachine = true ]]; then
		excluded="umurmur";
		for check in $excluded; do
			if [[ $file == "$check" ]]; then
				doNotLink=true;
			fi
		done
	fi
	
	if [[ ! $file = "." && ! $file = ".." && $doNotLink = false ]]; then
		if [[ ( -f $path/$file || -d $path/$file ) && ( ! -f $basePath/$file && ! -d $basePath/$file ) && ! $file = ".config" ]]; then
			$textG;
			printf "Linking %s...\n" "$file";
			$textR;
			ln -s "$path"/"$file" "$basePath"/;
			$textW;
			chown -h "$user":"$user" "$basePath"/"$file";
		elif [[ ( -f $path/$file || -d $path/$file ) && $file = ".config" ]]; then
			for a in "$path"/.config/*; do
				link "$a" true;
			done
			for a in "$path"/.config/.*; do
				link "$a" true;
			done
		fi
	fi
	$textW;
}

function installProgram {
	#TODO ssh failing for some reason for my suckless forks...
	#root owned the submodule folders, but manually taking ownership helped
	#TODO Move to new install bash scripts
	chown "$user":"$user" ./*;
	chown "$user":"$user" ./.*;
	if [[ -d $dirBuild/suckless/"$1" ]]; then
		$textG;
		printf "Installing %s...\n" "$1";
		$textR;
		cd "$dirBuild"/suckless/"$1"/;
		[[ -f $dirBuild/suckless/"$1"/config.h ]] && rm "$dirBuild"/suckless/"$1"/config.h;
		rm -f "$dirBuild"/suckless/"$1"/*.orig;
		rm -f "$dirBuild"/suckless/"$1"/*.rej;
		set -e;
		$textD;
		if [[ $1 = "slstatus" && $readingMachine = true ]]; then
			make alt
		elif [[ $1 = "slstatus" && $readingMachine = false ]]; then
			make normal;
		else
			make install;
		fi
		if [[ -x /usr/local/bin/$1 ]]; then
			rm /usr/local/bin/"$1";
		fi
		chown root:root ./"$1";
		cp ./"$1" /usr/local/bin/;
		$textC;
		printf "%s installed!\n\n" "$1";
		$textW;
	fi
}

function quit {
	$textW;
	printf "All done, customized configuration files have been linked and/or copied over.\n";
	printf "Don't forget to run "
	$textG;
	printf ":PlugInstall:"
	$textW;
	printf " in nvim to enable plugins.\n";
	printf "Don't forget to run "
	$textG;
	printf "lxappearance"
	$textW;
	printf " to setup QT/GTK2/3 theming.\n";
	$textC;
	printf "Enjoy your custom experience!\n";
	$textW;
	exit;
}

printf "Replacing pacman.conf...\n";
$textR;
if [[ -e /etc/pacman.conf && ! -e /etc/pacman.conf.old ]]; then
	mv /etc/pacman.conf /etc/pacman.conf.old;
fi
if [[ ! -e /etc/pacman.conf ]]; then
	ln -s "$dirEtc"/pacman.conf /etc/;
fi
chown -R root:root /etc/pacman.conf;
if [[ -d /etc/pacman.d/ && ! -e /etc/pacman.d/mirrorlist-arch ]]; then
	ln -s "$dirEtc"/pacman.d/mirrorlist-arch /etc/pacman.d/;
fi
$textW;
if [[ $skip = false ]]; then
	if [[ $force = false ]]; then
		pacman -Syu;
	else
		pacman -Syu --noconfirm;
	fi
fi

#TODO Add archlinux mirrorlist and artix support before essentials
#TODO pacman-key --populate archlinux

if [[ $skip = false ]]; then
	printf "Checking required packages...\n";
	requiredPackages="essential.txt";
	if [[ $barebones = true ]]; then
		requiredPackages="essential-barebones.txt";
	fi
	
	if [[ $force = false ]]; then
		cat $requiredPackages | pacman -S -
	else
		cat $requiredPackages | pacman -S --noconfirm -
	fi
	
	if pacman -Qg "base-devel" > /dev/null; then
		printf "base-devel group installed\n";
	else
		printf "base-devel group not installed...";
		if [[ $force = false ]]; then
			pacman -S "base-devel";
		else
			pacman -S --noconfirm "base-devel";
		fi
	fi
fi

if [[ $force = false ]]; then
	read -rp "Requirements met. Proceed? (Y|n) " answer;
	case $answer in
		n|N )	printf "Customizations not completed, answered no.\nAborting.\n"; exit;;
		* )		;;
	esac
fi

if [[ $skip = false ]]; then
	if [[ -e /usr/bin/yay ]]; then
		printf "yay installed, updating...\n";
		if [[ $skip = false ]]; then
			if [[ $force = false ]]; then
				sudo -u "$user" yay -Syu;
			else
				sudo -u "$user" yay -Syu --noconfirm;
			fi
		fi
	else
		cd /opt;
		if [[ ! -d /opt/yay-git ]]; then
			git clone https://aur.archlinux.org/yay-git.git;
			chown -R "$user":"$user" ./yay-git;
		fi
		cd yay-git;
		sudo -u "$user" makepkg -si;
		if [[ $skip = false ]]; then
			if [[ $force = false ]]; then
				sudo -u "$user" yay -Syu;
			else
				sudo -u "$user" yay -Syu --noconfirm;
			fi
		fi
	fi
	
	if [[ $force = false ]]; then
		read -rp "yay installed and updated. Install packages? (Y|n) " answer;
		case $answer in
			n|N )	printf "Customizations not completed, answered no.\nAborting.\n"; exit;;
			* )		;;
		esac
	fi
	
	cd "$SCRIPT_DIR";
	yayPackages="yay.txt";
	if [[ $barebones = true ]]; then
		yayPackages="yay-barebones.txt";
	fi
	
	#TODO Fix this? when asked to cleanbuild, errors out and fails. noconfirm
	#seems to work, removing force requirement for now.
	#if [[ $force = false ]]; then
	#	sudo -u "$user" grep -v "^#" $yayPackages | yay -S -
	#else
		sudo -u "$user" grep -v "^#" $yayPackages | yay -S --noconfirm -
	#fi
	
	if [[ $force = false ]]; then
		read -rp "yay finished updating. Proceed? (Y|n) " answer;
		case $answer in
			n|N )	printf "Customizations not completed, answered no.\nAborting.\n"; exit;;
			* )		;;
		esac
	fi
fi

#TODO Fix installation of font
# printf "Installing noto-color-emoji...\n";
# <?xml version="1.0" encoding="UTF-8"?>
# <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
#
# <!--
# Set Noto Color Emoji as fallback for Noto family
# -->
#
# <fontconfig>
#     <match target="scan">
#         <test name="family"><string>Noto Color Emoji</string></test>
#         <edit name="charset" mode="assign">
#             <minus>
#             <name>charset</name>
#                 <charset><range>
#                     <int>0x0000</int>
#                     <int>0x00FF</int>
#                 </range></charset>
#             </minus>
#         </edit>
#     </match>
#     <match>
#         <test name="family" compare="contains"><string>Noto </string></test>
#         <edit name="family" mode="append" binding="weak">
#             <string>Noto Color Emoji</string>
#         </edit>
#     </match>
# </fontconfig>

# fc-cache

printf "Replacing /etc/environment...\n";
$textR;
if [[ -f /etc/environment && ! -f /etc/environment.old ]]; then
	mv /etc/environment /etc/environment.old;
fi
if [[ -f /etc/environment && -f /etc/environment.old ]]; then
	mv /etc/environment /etc/environment.old;
fi
ln -s "$dirEtc"/environment /etc/;
chown -R root:root /etc/environment;
$textW;

printf "Packages installed (or skipped).\n";

if [[ $force = false ]]; then
	read -rp "Packages installed. Proceed? (Y|n) " answer;
	case $answer in
		n|N )	printf "Customizations not completed, answered no.\nAborting.\n"; exit;;
		* )		;;
	esac
fi

if [[ $barebones = false ]]; then
	printf "Initializing and updating submodules...\n";
	git submodule foreach "$(git submodule init)";
	git submodule foreach "$(git submodule update)";
	printf "Submodules up to date.\n";
fi

$textC;
printf "Running cleanup script...\n";
$textW;
bash "$SCRIPT_DIR"/cleanup.sh "$base";

if [[ $force = false ]]; then
	read -rp "Cleanup finished. Proceed? (Y|n) " answer;
	case $answer in
		n|N )	printf "Customizations not completed, answered no.\nAborting.\n"; exit;;
		* )		;;
	esac
fi

printf "\n";
$textC;
printf "Adding configuration files to %s\n" "$base";
$textW;

shopt -s dotglob;
if [[ -d $dirLink/ ]]; then
	if [[ ! -d $base/.config ]]; then
		mkdir "$base"/.config;
		chown -R "$user":"$user" "$base"/.config;
		$textY;
		printf ".config does not exist, creating...\n";
		$textW;
	fi
	for a in "$dirLink"/*; do
		link "$a" false;
	done
	for a in "$dirLink"/.*; do
		link "$a" false;
	done
	$textC;
	printf "Files linked!\n\n";
	$textW;
fi
shopt -u dotglob;

if [[ $barebones = true ]]; then
	quit;
fi

if [[ $force = false ]]; then
	read -rp "Ready to setup custom programs. Proceed? (Y|n) " answer;
	case $answer in
		n|N )	quit;;
		* )		;;
	esac
fi

if [[ -d $dirBin/ && -d "/usr/local/bin" ]]; then
	for file in "$dirBin"/*; do
		fileName=$(basename "$file");
		$textG;
		printf "Linking %s to /usr/local/bin/...\n" "$fileName";
		$textR;
		chown -h "${user:=$(/usr/bin/id -run)}":"$user" "$file";
		chmod +x "$file";
		ln -s "$file" /usr/local/bin/;
		$textW;
	done
	$textC;
	printf "Custom programs linked!\n\n";
	$textW;
fi

if [[ -d $dirBuild/fortune-mod-mormon ]]; then
	$textG;
	printf "Installing fortune-mod-mormon...\n";
	if [[ -d /usr/share/fortune/ ]]; then
		$textR;
		printf "Removing all other fortunes...\n";
		rm -r /usr/share/fortune/*;
		$textW;
	fi
	
	if [[ ! -d /usr/share/fortune/ ]]; then
		$textG;
		printf "Removing all other fortunes...\n";
		mkdir /usr/share/fortune;
		$textW;
	fi
	
	cd "$dirBuild"/fortune-mod-mormon/;
	set -e;
	make install;
	$textC;
	printf "fortune-mod-mormon installed!\n\n";
	$textW;
fi

if [[ $force = false ]]; then
	read -rp "Ready to setup custom runit services. Proceed? (Y|n) " answer;
	case $answer in
		n|N )	quit;;
		* )		;;
	esac
fi

if [[ -d "$dirRunit"/ ]]; then
	$textR;
	if [[ ! -d "/etc/runit" ]]; then
		mkdir /etc/runit;
	fi
	if [[ ! -d "/run/runit" ]]; then
		mkdir /run/runit;
	fi
	if [[ ! -d "/run/runit/serivce" ]]; then
		mkdir /run/runit/service;
		ln -sf /etc/runit/runsvdir/current /run/runit/service;
	fi
	for file in "$dirRunit"/*; do
		fileName=$(basename "$file");
		$textG;
		printf "Linking %s to /usr/local/bin/...\n" "$fileName";
		$textR;
		chown -h "${user:=$(/usr/bin/id -run)}":"$user" "$file";
		chmod +x "$file";
		ln -sf "$file" /etc/runit/sv/;
		$textW;
		rsm enable "$fileName";
		
		if [[ $force = false ]]; then
			read -rp "Start service $fileName? (Y|n) " answer;
			case $answer in
				n|N )	;;
				* )		rsm start "$fileName";;
			esac
		else
			rsm start "$fileName";
		fi
	done
	$textC;
	printf "Custom runit services linked!\n\n";
	$textW;
fi

alsaDown=$(eval echo "$(sudo sv status alsa | { grep -q 'down:'; echo $?; })");
if [[ $alsaDown -gt 0 ]]; then
	rsm enable alsa;
	rsm start alsa;
fi

nmDown=$(eval echo "$(sudo sv status NetworkManager | { grep -q 'down:'; echo $?; })");
if [[ $nmDown -gt 0 ]]; then
	rsm enable NetworkManager;
	rsm start NetworkManager;
fi

if [[ $force = false ]]; then
	read -rp "Ready to install suckless programs. Proceed? (Y|n) " answer;
	case $answer in
		n|N )	quit;;
		* )		;;
	esac
fi

installProgram "dwm";
installProgram "dmenu";
installProgram "slstatus";
installProgram "st";

quit;
