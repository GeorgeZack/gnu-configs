#!/bin/bash
printf "\n";
if [ "$EUID" -ne 0 ]; then
	printf "Please run as root\n";
	exit;
fi

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )";
dirLink="$SCRIPT_DIR/link";
dirBin="$SCRIPT_DIR/bin";

while getopts "o:f" flag; do
	case "${flag}" in
		o ) override=${OPTARG};;
		* ) ;;
	esac
done

if [[ -e $override ]] && [[ -d $override ]]; then
	base=$override;
else
	base=$(eval echo ~"$(logname)");
fi

function deleteFile {
	file=$(basename "$1");
	path="$dirLink";
	basePath="$base";
	
	if [[ $2 = true ]]; then
		path="$dirLink"/.config;
		basePath="$base"/.config;
	fi
	
	if [[ ! $file = "." && ! $file = ".." ]]; then
		if [[ ( -f $path/$file || -d $path/$file ) && ( -f $basePath/$file || -d $basePath/$file ) && ! $file = ".config" ]]; then
			tput setaf 3;
			printf "Removing %s...\n" "$file";
			tput setaf 1;
			rm -r "${basePath:?}"/"$file";
			tput setaf 7;
		elif [[ ( -f $basePath/$file || -d $basePath/$file ) && $file = ".config" ]]; then
			for a in "$path"/.config/*; do
				deleteFile "$a" true;
			done
			for a in "$path"/.config/.*; do
				deleteFile "$a" true;
			done
		fi
	fi
	tput setaf 7;
}

printf "Cleaning configuration files from %s\n" "$base";
printf "(Don't worry, only configuration files that are in the git repo will be removed. Other files will NOT be untouched.)\n";

shopt -s dotglob;
if [[ -d $dirLink/ ]]; then
	for a in "$dirLink"/*; do
		deleteFile "$a" false;
	done
	for a in "$dirLink"/.*; do
		deleteFile "$a" false;
	done
	tput setaf 6;
	printf "Linked files removed!\n\n";
	tput setaf 7;
fi
shopt -u dotglob;

if [[ -d $dirBin/ ]] && [[ -d "/usr/local/bin" ]]; then
	tput setaf 7;
	printf "bin exists, checking children...\n";
	for a in "$dirBin"/*; do
		file=$(basename "$a");
		if [[ -e /usr/local/bin/$file ]]; then
			tput setaf 3;
			printf "Removing %s in /usr/local/bin/ directory...\n" "$file"
			tput setaf 1;
			rm "/usr/local/bin/$file";
			tput setaf 7;
		fi
	done
	tput setaf 6;
	printf "/usr/local/bin/ cleaned!\n";
	tput setaf 7;
fi

# Unintsall suckless software

tput setaf 6;
printf "Cleaned up!\n";
tput setaf 7;
