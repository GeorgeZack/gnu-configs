# Vim/Nvim Notes

## Leader Key

`\`

## Movement

`H` `J` `K` `L` - All move 10 spaces in the direction

`e` `b` - Move forward and back a word

## Common Keys

`i` - Inserting before the cursor\
`a` - After the cursor\
`I` - At the beginning of the line\
`A` - End of the line\
`S` - In the correct indentation level of an empty line\
`o` - Create a new line below\
`O` - Create a new line above\
`daw` - Delete word under cursor\
`caw` - Delete word under cursor and enter insert mode

## Search

Search with `/` key, add `\c` to disable case-sensitivity.

## Indent

`>>` - Indent\
`<<` - Unindent

## Comment

### Normal mode

`gcc` - Toggles the current line using linewise comment\
`gbc` - Toggles the current line using blockwise comment\
`[count]gcc` - Toggles the number of line given as a prefix-count using
linewise\
`[count]gbc` - Toggles the number of line given as a prefix-count using
blockwise\
`gc[count]{motion}` - (Op-pending) Toggles the region using linewise comment\
`gb[count]{motion}` - (Op-pending) Toggles the region using blockwise comment

### Visual mode

`gc` - Toggles the region using linewise comment\
`gb` - Toggles the region using blockwise comment

### Remove Comment after new line

`<Ctrl-U>` - Removes the comment characters after pressing a newline key (`o`,
`O`, `<leader>O`, etc.)

## Sort

Using Visual mode (especially Visual-Line) highlight the words wanting to sort.
Then press `:` key, then type `sort`.

## Spell

`:zg` - Add word under cursor as a good word\
`:z=` - Suggest correctly spelled words under cursor\
`]s` - Move to next misspelled word\
`[s` - Move to previous misspelled word

## Folding
(Capital letters will toggle fold recursively)

`za` `zA` - Toggle\
`zo` `zO` - Open\
`zr` `zR` - Open all\
`zc` `zC` - Close\
`zm` `zM` - Close all

## True Zen

`:TZAtaraxis` - For that good stuff

## VimTeX

`\ll` - To toggle compiling\
`\lv` - To view compiled document

## Trouble!

`\t` - To toggle error diagnostics

## Arial

`\a` - Toggle

## Telescope

`\ff` - Files\
`\fg` - Grep

## Save as Sudo

`w!!` - In normal mode

## Lists

Using `bullets` plugin:

### Promotion

`<C-d>` - Insert mode\
`<<` - Normal mode\
`<` - Visual mode

### Demotion

`<C-t>` - Insert mode\
`>>` - Normal mode\
`>` - Visual mode

## Viewing Where a Setting was Changed

`:verbose set [option]?`

## Selection

`<C-v>`, `j/k`, `$` - Enter visual-block, expand block, extend to the end of
each line\
`:%norm A[characters]` or with selection `:norm A[characters]` - Insert
characters at end of each line

## Tables

Using `vim-table-mode` plugin:

`\tm` - Toggle table mode\
`\tdd` - Delete row\
`\tdc` - Delete column\
`\tic` - Insert column

## Change case

While in Visual mode:

`u` for lowercase\
`U` for uppercase
