# Personal Setup Script for Artix/Arch Linux
##### By George Zackrison

GPL Version 3, see license for more details.

## Notes
Configuration directories, files (including dotfiles), and programs for a custom
GNU/Linux experience on Artix.

Note that this has been tested on Artix and Arch. The normal setup (no optional
flags enabled) is meant strictly for Artix as it includes **runit** files and
services, though I don't think it would break an Arch system.

## Prerequisites
- Installation of Artix (following the guide included)
- Requires a user (that is not root) with permission to use `sudo`
	- Preferably a user in the `wheel` group

To do so:
1. First create a user
2. Elevate user to `sudo` or `wheel` group
3. Login to user
4. Proceed with installation

## Installation
Run the `setup.sh` file as root with bash to get started.

## Cleanup
If you'd like to cleanup the configuration files and not set anything up, simply
run `cleanup.sh` with bash.
